select title from movies where budget > 10000000 and ranking < 6;
select movies.movie_id, title from movies join genres on movies.movie_id = genres.movie_id where rating > 8.0 and year > 2000 and genre_name = "action";
select title from movies join genres on movies.movie_id = genres.movie_id where duration > 150 and oscars > 2;

select title
from movies 
where oscars > 2 and movie_id in( 
	select movie_id 
    from movie_stars join stars on movie_stars.star_id = stars.star_id 
    where star_name = "Orlando Bloom" and movie_id in(
		select movie_id 
        from movie_stars join stars on movie_stars.star_id = stars.star_id 
        where star_name = "Ian McKellen"
	)
);