-- Customer Deletion

select * from Customers;

select * from Orders where CustomerID = 20;

select OrderID from OrderDetails where OrderID in(
	select OrderID from Orders where CustomerID = 20
);

delete from OrderDetails where OrderID in(
	select OrderID from Orders where CustomerID = 20
);

delete from Orders where CustomerID = 20;

delete from Customers where CustomerID = 20;

select * from Customers where CustomerID = 20;

-- Product Deletion // Deleted Product 5

select * from Products;

select * from Products where ProductID = 5;

select * from OrderDetails where ProductID in(
	select ProductID from Products where ProductID = 5
);

delete from OrderDetails where ProductID = 5;
delete from Products where ProductID = 5;

select * from Products where ProductID = 5;

-- Shipper Deletion

select * from Shippers;
select OrderID from Orders where ShipperID = 2;

select * from OrderDetails where OrderID in(
	select OrderID from Orders where ShipperID = 2
);

delete from OrderDetails where OrderID in(
	select OrderID from Orders where ShipperID = 2
);

delete from Orders where ShipperID = 2;

delete from Shippers where ShipperID = 2;


