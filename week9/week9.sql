create view usa_customers as
select CustomerID, CustomerName, ContactName from Customers where Country = "USA" ; 

select * from usa_customers;

select ProductID, ProductName, Price from Products where Price < (select avg(Price) from Products);

create view product_below_avg_price as
select ProductID, ProductName, Price from Products where Price < (select avg(Price) from Products);

select * from product_below_avg_price;

select * from usa_customers join Orders on usa_customers.CustomerID = Orders.CustomerID where OrderID in (
	select OrderID from OrderDetails join product_below_avg_price on OrderDetails.ProductID = product_below_avg_price.ProductID
);

create or replace view product_below_avg_price as
select ProductID, ProductName, Price from Products where Price < 30;

select * from product_below_avg_price;

drop view usa_customers;
